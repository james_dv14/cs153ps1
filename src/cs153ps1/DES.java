package cs153ps1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class DES {
    public static final int BLOCK_SIZE = 64;
    public static final int ROUNDS = 16;
    /**
     * Encrypts or decrypts a block of data based on the Digital Encryption 
     * Standard (DES).
     * @param data The plaintext to encrypt, or ciphertext to decrypt.
     * @param key The key (with parity bits).
     * @param decrypt True decrypts, false encrypts.
     * @return The ciphertext (if encryption) or plaintext (if decryption).
     */
    public static byte[] execute(byte[] data, byte[] key, boolean decrypt) {
        Subkey subkeys = new Subkey(key);
        // generate all round subkeys
        subkeys.generate(decrypt); 
        // inital permutation
        byte result[] = Permutation.permute(data, Permutation.IP);  
        // split data into left and right halves
        byte halves[][] = BitOps.splitIntoBlocks(result, BLOCK_SIZE / 2); 
        for (int i = 0; i < ROUNDS; i++) {
            // next left half becomes current right half
            byte[] left = halves[1]; 
            // current right half is fed into F-function
            byte[] f_result = F_Function.execute(halves[1], subkeys.get(i));
            // next right half is the XOR result of the current left half and the result of the F-function 
            byte[] right = BitOps.xorBlocks(halves[0], BLOCK_SIZE / 2, f_result, BLOCK_SIZE / 2); 
            halves[0] = left;
            halves[1] = right;
        }
        // swap left and right halves once more
        result = BitOps.concatenateBlock(halves[1], BLOCK_SIZE / 2, halves[0], BLOCK_SIZE / 2); 
        // inverse of initial permutation
        result = Permutation.permute(result, Permutation.FP); 
        return result;
    }
    /**
     * Encrypts a block of data based on the Digital Encryption Standard (DES).
     * @param plaintext The plaintext to encrypt.
     * @param key The key (with parity bits).
     * @return The ciphertext block.
     */
    public static byte[] encrypt(byte[] plaintext, byte[] key) {
        return execute(plaintext, key, false);
    }
    /**
     * Decrypts a block of data based on the Digital Encryption Standard (DES).
     * @param ciphertext The ciphertext to decrypt.
     * @param key The key (with parity bits).
     * @return The plaintext block.
     */
    public static byte[] decrypt(byte[] ciphertext, byte[] key) {
        return execute(ciphertext, key, true);
    }
}
