package cs153ps1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class BitOps {
    /**
     * Returns the difference of a block's size, and the smallest multiple of 8 
     * that is greater than or equal to its size.
     * @param block The block.
     * @param size The block size in bits.
     * @return 
     */
    public static int getBlockOffset(byte[] block, int size) {
        return block.length * 8 - size;
    }
    /**
     * Extracts the bit at a specific position in a byte.
     * @param data The byte from which to extract a bit.
     * @param i The index of the bit. (where 0 is the most significant bit)
     * @return The extracted bit.
     */
    public static int getBit(byte data, int i) {
        return data >> (7 - i % 8) & 0x0001;
    }
    /**
     * Extracts the bit at a specific position in a byte array.
     * @param data The byte array from which to extract a bit.
     * @param i The index of the bit. (where 0 is the most significant bit in the entire array)
     * @return The extracted bit.
     */
    public static int getBit(byte[] data, int i) {
        return getBit(data[i/8], i);
    }
    /**
     * Extracts the bit at a specific position in a block.
     * @param data The byte array from which to extract a bit.
     * @param i The index of the bit. (where 0 is the most significant bit in the entire array)
     * @param size The block size in bits.
     * @return The extracted bit.
     */
    public static int getBit(byte[] data, int i, int size) {
        int offset = getBlockOffset(data, size);
        return getBit(data[(i + offset)/8], i + offset);
    }
    /**
     * Sets the bit at a specific position in a byte to 0.
     * @param data The byte to be modified.
     * @param i The index of the bit. (where 0 is the most significant bit)
     * @return The modified copy of the original byte.
     */
    public static byte setBitToZero(byte data, int i) {
        int mask = 0xFF7F >> (i % 8);
        return (byte)(data & mask);
    }
    /**
     * Sets the bit at a specific position in a byte.
     * @param data The byte to be modified.
     * @param i The index of the bit. (where 0 is the most significant bit)
     * @param value The value to which the bit will be set.
     * @return The modified copy of the original byte.
     */
    public static byte setBit(byte data, int i, int value) {
        value = value > 0 ? 1 : 0;
        data = setBitToZero(data, i);
        int mask = value << (7 - i % 8);
        return (byte)(data | mask);
    }
    /**
     * Sets the bit at a specific position in a byte array.
     * @param data The byte array to be modified.
     * @param i The index of the bit. (where 0 is the most significant bit in the entire array)
     * @param value The value to which the bit will be set.
     */
    public static void setBit(byte[] data, int i, int value) {
        data[i/8] = setBit(data[i/8], i, value);
    }
    /**
     * Sets the bit at a specific position in a block.
     * @param data The byte array to be modified.
     * @param i The index of the bit. (where 0 is the most significant bit in the entire array)
     * @param size The block size in bits.
     * @param value The value to which the bit will be set.
     */
    public static void setBit(byte[] data, int i, int value, int size) {
        int offset = getBlockOffset(data, size);
        data[(i+ offset)/8] = setBit(data[(i+ offset)/8], i + offset, value);
    }
    /**
     * Splits an array of bytes into blocks of a specified size.
     * @param data The byte array to be split.
     * @param size The block size in bits.
     * @return A 2-dimensional byte array containing blocks of the specified 
     * size. The first element of a block is a byte containing UP TO the 8 most 
     * significant bits, while the succeeding elements (if the block size 
     * exceeds 8) are bytes containing EXACTLY 8 less significant bits 
     * each.
     */
    public static byte[][] splitIntoBlocks(byte[] data, int size) {
       int block_count = (8 * data.length - 1) / size + 1;
       int bytes_per_block = (size - 1) / 8 + 1 ;
       byte[][] blocks = new byte[block_count][bytes_per_block];
       int offset = getBlockOffset(blocks[0], size);
       for (int i = 0; i < block_count; i++) {
          byte[] block_t = new byte[bytes_per_block];
          for (int j = 0; j < size; j++) {
             if (i * bytes_per_block + j/8 == data.length ) {
                 break;
             }
             int value = getBit(data, i * size + j); 
             block_t[(j + offset)/8] = setBit(block_t[(j + offset)/8], j + offset, value);
          }
          blocks[i] = block_t;
       }
       return blocks;
    }
    /**
     * Splits an array of bytes into blocks of size 8 bits or less.
     * @param data The byte array to be split.
     * @param size The block size in bits.
     * @return A byte array containing elements of the given block size. If the
     * block size is less than 8, the most significant bit(s) of each element
     * are set to 0. If the block size is 8 or greater, the original byte array
     * is returned.
     */
    public static byte[] splitIntoSmallBlocks(byte[] data, int size) {
        if (size >= 8) {
            return data;
        }
        byte[][] largeBlocks = splitIntoBlocks(data, size);
        byte[] smallBlocks = new byte[largeBlocks.length];
        for (int i = 0; i < largeBlocks.length; i++) {
            smallBlocks[i] = largeBlocks[i][0];
        }
        return smallBlocks;
    }
    /**
     * Rotates a block in a given direction by a specified number of bits.
     * @param block The block to be rotated, as a byte array.
     * @param size The block size in bits.
     * @param bits The number of bits to shift / rotate.
     * @param left True rotates left, false rotates right.
     * @return The rotated block, as a byte array.
     */
    public static byte[] rotateBlock(byte[] block, int size, int bits, boolean left) {
        int byte_count = block.length;
        int rotation = (left ? 1 : -1) * bits;
        byte[] result = new byte[byte_count];
        for (int i = 0; i < size; i++) {
           int value = getBit(block, Helper.getPositiveModulus(i + rotation, size), size);
           setBit(result, i, value, size);
        }
        return result;
    }
    /**
     * Rotates a block left by a specified number of bits.
     * @param block The block to be rotated, as a byte array.
     * @param size The block size in bits.
     * @param bits The number of bits to shift / rotate.
     * @return The rotated block, as a byte array.
     */
    public static byte[] rotateBlockLeft(byte[] block, int size, int bits) {
        return rotateBlock(block, size, bits, true);
    }
    /**
     * Rotates a block right by a specified number of bits.
     * @param block The block to be rotated, as a byte array.
     * @param size The block size in bits.
     * @param bits The number of bits to shift / rotate.
     * @return The rotated block, as a byte array.
     */
    public static byte[] rotateBlockRight(byte[] block, int size, int bits) {
        return rotateBlock(block, size, bits, false);
    }
    /**
     * Concatenates two blocks into a single block.
     * @param block_a The first block.
     * @param size_a The size of the first block in bits.
     * @param block_b The second block.
     * @param size_b The size of the second block in bits.
     * @return The concatenated block as a byte array. The first element is a 
     * byte containing UP TO the 8 most significant bits of the first block, 
     * while the succeeding elements (if the total block size exceeds 8) are 
     * bytes each containing EXACTLY 8 less significant bits of the first block
     * and / or bits of the second block.
     */
    public static byte[] concatenateBlock(byte[] block_a, int size_a, byte[] block_b, int size_b) {
        int size = size_a + size_b;
        int byte_count = (size - 1) / 8 + 1;
        byte[] result = new byte[byte_count];
        int offset = getBlockOffset(result, size);
        int offset_a = getBlockOffset(block_a, size_a);
        int offset_b = getBlockOffset(block_b, size_b);
        int j = 0;
        for (int i = 0; i < size_a; i++) {
            int value = getBit(block_a, i + offset_a);
            setBit(result, j + offset, value);
            j++;
        }
        for (int i = 0; i < size_b; i++) {
            int value = getBit(block_b, i + offset_b);
            setBit(result, j + offset, value);
            j++;
        }
        return result;
    }
    /**
     * Gets the result of the XOR operation of two blocks.
     * @param block_a The first block.
     * @param size_a The size of the first block in bits.
     * @param block_b The second block.
     * @param size_b The size of the second block in bits.
     * @return The result of the XOR operation, as a byte array. If one block
     * is larger, the result only contains as many bits as the smaller block.
     * The less significant bits of the larger block are discarded.
     */
    public static byte[] xorBlocks(byte[] block_a, int size_a, byte[] block_b, int size_b) {
        int size = Math.min(size_a, size_b);
        int byte_count = (size - 1) / 8 + 1;
        byte[] result = new byte[byte_count];
        for (int i = 0; i < size; i++) {
            int value_a = getBit(block_a, i, size_a);
            int value_b = getBit(block_b, i, size_b);
            int value = value_a ^ value_b;
            setBit(result, i, value, size);
        }
        return result;
    }
}
