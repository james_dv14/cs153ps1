package cs153ps1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class DESOutput {
    File file;
    FileOutputStream fos;
    PrintWriter output;
            
    public DESOutput (String filepath) {
        file = new File(filepath);
        try {
            fos = new FileOutputStream(file);
            output = new PrintWriter(new FileWriter(file, false));
        } 
        catch (FileNotFoundException ex) {
            Logger.getLogger(DESOutput.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DESOutput.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean writeBlockFrom(byte[] block, String repr) {
        //System.out.println("toFile:" + Helper.byteArrayToHexString(block, false));
        try {
            switch (repr) {
                case "%h":
                    output.print(Helper.byteArrayToHexString(block, false));
                    return true;
                case "%b":
                    output.print(Helper.byteArrayToBinaryString(block, false));
                    return true;
                default:
                    fos.write(block);
                    return true;
            }
        } 
        catch (IOException ex) {
            Logger.getLogger(DESOutput.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }
    
    public void close() {
        try {
            output.close();
            fos.close();
        } 
        catch (IOException ex) {
            Logger.getLogger(DESOutput.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
