package cs153ps1;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HashMap<String, String> options = processArgs(args);
        boolean good;
        
        DESInput input = new DESInput(options.get("-i"));
        DESInput keyfile = new DESInput(options.get("-k"));
        DESOutput output = new DESOutput(options.get("-o"));
        
        
        byte[] key = new byte[8];
        byte[] buffer = new byte[8];
        keyfile.readNextBlockTo(key, options.get("-k_repr"), 8);
        good = keyfile.exact;
        if (!good) {
        }
        
        Subkey subkeys = new Subkey(key);
        subkeys.generate();
        for (int i = 0; i < subkeys.length(); i++) {
            System.out.println("Subkey for round " + (i+1) + ": " + 
                    Helper.byteArrayToHexString(subkeys.get(i), false));
        }
        
        while(good) {
            input.readNextBlockTo(buffer, options.get("-i_repr"), 0);
            //System.out.println("block:" + Helper.byteArrayToHexString(buffer, false));
            /*
            if ( Helper.byteArrayToHexString(buffer, false).equals("0000000000000000") ) {
                break;
            }*/
            good = good && !input.eof;
            byte[] block = DES.execute(buffer, key, options.containsKey("-D"));
            //System.out.println("block:" + Helper.byteArrayToHexString(block, false));
            good = good && output.writeBlockFrom(block, options.get("-o_repr"));
        }
        
        input.close();
        keyfile.close();
        output.close();
    }
    
    public static HashMap processArgs(String[] args) {
        String file_mode = "";
        HashMap<String, String> options = new HashMap();
        options.put("-i_repr", "%a");
        options.put("-k_repr", "%h");
        options.put("-o_repr", "%a");
        for (String arg : args) {
           if (arg.substring(0, 2).equals("-i") || 
                   arg.substring(0, 2).equals("-k") || 
                   arg.substring(0, 2).equals("-o")) {
               file_mode = arg.substring(0, 2);
               if (arg.length() > 2) {
                   String repr = arg.substring(2, arg.length());
                   options.put(file_mode + "_repr", repr);
               }
           }
           else if (arg.equals("-D")) {
               options.put(arg, "");
           }
           else {
               options.put(file_mode, arg);
           }
        }
        return options;
    }
}
