package cs153ps1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Subkey {
    byte[] key;
    byte[][] subkeys;
    
    public Subkey(byte[] key) {
        this.key = key;
    }
    
    /**
     * Populates the internal subkey array with subkeys generated in accordance
     * with the Digital Encryption Standard (DES).
     * @param decrypt True will generate the keys in reverse order.
     */
    public void generate(boolean decrypt) {
        int half_length = Permutation.PC1.length / 2;
        byte[] whole;
        byte[][] halves;
        subkeys = new byte[DES.ROUNDS][];
        // permuted choice 1
        whole = Permutation.permute(key, Permutation.PC1);
        for (int i = 0; i < DES.ROUNDS; i++) {
            // split subkey into 28-bit halves
            halves = BitOps.splitIntoBlocks(whole, half_length); 
            int rotate = (i < 2 || i == 8 || i == 15) ? 1 : 2;
            // rotate half C
            halves[0] = BitOps.rotateBlockLeft(halves[0], half_length, rotate); 
            // rotate half D
            halves[1] = BitOps.rotateBlockLeft(halves[1], half_length, rotate); 
            // rejoin halves
            whole = BitOps.concatenateBlock(halves[0], half_length, halves[1], half_length); 
            int index = decrypt ? DES.ROUNDS - 1 - i : i;
            // permuted choice 2
            subkeys[index] = Permutation.permute(whole, Permutation.PC2); 
        }
    }
    /**
     * Generates subkeys for encryption following the Digital Encryption Standard
     * (DES).
     */
    public void generate() {
        generate(false);
    }
    /**
     * Gets the number of subkeys.
     * @return The number of subkeys.
     */
    public int length() {
        return subkeys.length;
    }
    /**
     * Gets a subkey at a specified index.
     * @param index An index i.
     * @return The i-th subkey.
     */
    public byte[] get(int index) {
        return subkeys[index];
    }
            
}
