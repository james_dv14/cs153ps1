package cs153ps1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Helper {
    final protected static char[] hex = "0123456789ABCDEF".toCharArray();
    /**
     * Gets the positive result of a modulus operation, even with a negative
     * dividend.
     * @param a The dividend.
     * @param b The divisor.
     * @return The positive result of the modulus operation.
     */
    public static int getPositiveModulus (int a, int b) {
        return (a % b + b) % b;
    }
    /**
     * Gets the binary string representation of a byte.
     * @param b A byte.
     * @param embellish True prepends "0b" to the output and adds spaces every 4 
     * digits.
     * @return The binary string representation.
     */
    public static String byteToBinaryString(byte b, boolean embellish) {
        String retstr = embellish ? "0b" : "";
        for (int i = 0; i < 8; i++) {
            int value = BitOps.getBit(b, i);
            retstr += value;
            retstr += i % 4 == 3 && embellish ? " " : "";
        }
        return retstr;
    }
    /**
     * Gets the binary string representation of a byte array.
     * @param data A byte array.
     * @param embellish True prepends "0b" to the output and adds spaces every 
     * 4 digits.
     * @return The binary string representation.
     */
    public static String byteArrayToBinaryString (byte[] data, boolean embellish) {
        String retstr = embellish ? "0b" : "";
        for (int i = 0; i < data.length; i++) {
            retstr += byteToBinaryString(data[i], false);
        }
        return retstr;
    }
    /**
     * Gets the hexadecimal string representation of a byte.
     * @param b A byte.
     * @param embellish True prepends "0x" to the output and adds spaces every 
     * 4 digits.
     * @return The hexadecimal string representation.
     */
    public static String byteToHexString(byte b, boolean embellish) {
        String retstr = embellish ? "0x" : "";
        char[] hexChar = new char[2];
        int value = b & 0xFF;
        hexChar[0] = hex[value >>> 4];
        hexChar[1] = hex[value & 0x0F];
        retstr += new String(hexChar);
        return retstr;
    }
    /**
     * Gets the hexadecimal string representation of a byte array.
     * @param data A byte array.
     * @param embellish True prepends "0x" to the output and adds spaces every 
     * 4 digits.
     * @return The hexadecimal string representation.
     */
    public static String byteArrayToHexString (byte[] data, boolean embellish) {
        String retstr = embellish ? "0x" : "";
        for (int i = 0; i < data.length; i++) {
            retstr += byteToHexString(data[i], false);
            retstr += i % 2 == 1 && embellish ? " " : "";
        }
        return retstr;
    }
}
