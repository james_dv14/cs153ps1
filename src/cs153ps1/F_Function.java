package cs153ps1;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class F_Function {
    /**
     * Gets the result of the DES F-function given a 32-bit block and a 48-bit
     * round subkey.
     * @param half The (right) half of the block from the previous round.
     * @param subkey The round subkey.
     * @return The result of the DES F-function.
     */
    public static byte[] execute(byte[] half, byte[] subkey) {
        // expansion
        byte[] result = Permutation.permute(half, Permutation.E);
        // xor expanded (half) block with subkey
        result = BitOps.xorBlocks(result, result.length * 8, subkey, subkey.length * 8);
        // split result of xor into blocks of 6 bits each
        byte[] sixBits = BitOps.splitIntoSmallBlocks(result, 6);
        // use S-boxes to obtain 4-bit blocks from 6-bit blocks
        byte[][] fourBits = new byte[sixBits.length][1];
        for (int i = 0; i < sixBits.length; i++) {
            fourBits[i][0] = Substitution.substitute(sixBits[i], i);
        }
        // concatenate all 4-bit blocks
        result = BitOps.concatenateBlock(fourBits[0], 4, fourBits[1], 4);
        for (int i = 2; i < fourBits.length; i++) {
            result = BitOps.concatenateBlock(result, 4 * (i), fourBits[i], 4);
        }
        // permutation
        result = Permutation.permute(result, Permutation.P);
        return result;
    }
}
