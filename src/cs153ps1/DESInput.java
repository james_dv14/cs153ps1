package cs153ps1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class DESInput {
    File file;
    FileInputStream fis;
    boolean eof = false; // end of file has been reached
    boolean exact = true; // input is exact block size
            
    public DESInput (String filepath) {
        file = new File(filepath);
        try {
            fis = new FileInputStream(file);
        } 
        catch (FileNotFoundException ex) {
            Logger.getLogger(DESInput.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean eof() { return eof; }
    public boolean exact() { return exact; }
    
    public int readNextByteAsPowerOfTwoTo(byte[] block, int index, int power)
            throws IOException, NumberFormatException {
        int bytes_read = 0;
        char[] byte_chars = new char[8/power];
        int tmp;
        for(int i = 0; i < byte_chars.length; i++) {
            tmp = fis.read();
            if (tmp == -1) {
                byte_chars[i] = '0';
                eof = true;
            }
            else {
                byte_chars[i] = (char) tmp;
                bytes_read++;
            }
        }
        String byte_string = new String(byte_chars);
        block[index] = (byte) Integer.parseInt(byte_string, (int)(Math.pow(2.0, (double)power)));
        return bytes_read;
    }
    public int readNextByteAsBinaryTo(byte[] block, int index) 
            throws IOException, NumberFormatException {
        return readNextByteAsPowerOfTwoTo(block, index, 1);
    }
    
    public int readNextByteAsHexTo(byte[] block, int index) 
            throws IOException, NumberFormatException {
        return readNextByteAsPowerOfTwoTo(block, index, 4);
    }
    
    public void readNextBlockTo(byte[] block, String repr, int size) {
        int bytes_read;
        exact = true;
        try {
            switch (repr) {
                case "%h":
                    for (int i = 0; i < block.length; i++) {
                        if (eof) {
                            block[i] = 0;
                        }
                        else {
                            bytes_read = readNextByteAsHexTo(block, i);
                            exact = bytes_read == 0 || bytes_read == 2;
                            eof = bytes_read < 2;
                        }
                    }
                    break;
                case "%b":
                    for (int i = 0; i < block.length; i++) {
                        if (eof) {
                            block[i] = 0;
                        }
                        else {
                            bytes_read = readNextByteAsBinaryTo(block, i);
                            exact = bytes_read == 0 || bytes_read == 8;
                            eof = bytes_read < 8;
                        }
                    }
                    break;
                default:    
                    bytes_read = fis.read(block);
                    if (size > 0) { 
                        exact = bytes_read == size;
                    }
                    eof = bytes_read == -1;
            }
        }
        catch (IOException | NumberFormatException ex) {
            Logger.getLogger(DESInput.class.getName()).log(Level.SEVERE, null, ex);
            eof = true;
            exact = false;
        }
    }
    
    public void close() {
        try {
            fis.close();
        } 
        catch (IOException ex) {
            Logger.getLogger(DESInput.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
