CS 153 PS 1
================
**Command-line arguments:**

-k -- Precedes the path of the key file.

-i -- Precedes the path of the input file.

-o -- Precedes the path of the output file.

-[kio]%a -- Reads/writes the file as ASCII (default).

-[kio]%b -- Reads/writes the file as binary.

-[kio]%h -- Reads/writes the file as hex.

**Modes:**

-D -- Decryption (generate subkeys in reverse order).

**Sample usage:**

$ -k "key" -i "plaintext" -o "ciphertext"

$ -D -k%h "key" -i%h "ciphertext" -o%h "plaintext2"